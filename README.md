# How to use
## Short-Process
Git clone: git clone https://sindhuanurag@bitbucket.org/sindhuanurag/hindii-shbdsaagr.git

Move in to: hindii-shbdsaagr/हिन्दी-शब्दसागर

Execute: 
```
make install
```

Validate via launching Dictionary app.

----
## Long-Process
Convert Hindi Shabdsagar to Apple Dictionary so as to use with its Dictionary app
1. Create a folder like
   ```
	mkdir ~/Developer/Source/scratchpad
   ```

2. Move in to this folder
   ```
	cd ~/Downloads/scratchpad
   ```

3. Download and copy *हिन्दी-शब्दसागर.txt* file in there

4. Ensure that you have downloaded pyglossary
   Create a folder: mkdir ~/Developer/Source/pyglossary
   
   Move in there: 
   ```
   cd ~/Developer/Source/pyglossary
   ```
   
   Git clone: git clone https://github.com/ilius/pyglossary.git

   Ensure that you have correct version of python installed. You may use homebrew to install if it is missing.

5. Download and extract "Additional Tools for Xcode" from http://developer.apple.com/downloads
   You need to have developer access to download from there. You may extract contents of this at: *~/Developer/Environment/xcode-additional-tools*

6. Move in to scratchpad directory created in step 2
   ```
   cd ~/Downloads/scratchpad
   ```

7. Convert to Apple Dictionary format
   python3 ~/Developer/Source/pyglossary/main.py --write-format=AppleDict "हिन्दी-शब्दसागर.txt" "हिन्दी-शब्दसागर"
   ```
   cd "हिन्दी-शब्दसागर"
   ```

   Open **makefile** and edit ```DICT_BUILD_TOOL_DIR``` value to point to "Dictionary Development Kit"
   ```DICT_BUILD_TOOL_DIR	=	"/Users/ANu/Developer/Environment/xcode-additional-tools/Utilities/Dictionary Development Kit"```
   
   ```
   make
   ```

   ```
   make install
   ```

8. Validate via opening Dictionary app



